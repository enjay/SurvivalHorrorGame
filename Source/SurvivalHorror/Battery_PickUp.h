// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp_Base.h"
#include "Battery_PickUp.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API ABattery_PickUp : public APickUp_Base
{
	GENERATED_BODY()
		ABattery_PickUp();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PickUpValue", meta = (AllowPrivateAccess = "true"))
		float addBattery;
	virtual void OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;

};

// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SurvivalHorrorCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/SkeletalMesh.h"
#include "Components/SkeletalMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/SpotLightComponent.h"
#include "EquipedItem_Base.h"

//////////////////////////////////////////////////////////////////////////
// ASurvivalHorrorCharacter

ASurvivalHorrorCharacter::ASurvivalHorrorCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = true;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;
	//Set SkeletalMesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh>SkeletalMesh(TEXT("/Game/Mannequin/Character/Mesh/SK_Mannequin"));
	if (SkeletalMesh.Succeeded())
		GetMesh()->SetSkeletalMesh(SkeletalMesh.Object);
	static ConstructorHelpers::FClassFinder<UAnimInstance> Anim_BP(TEXT("/Game/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP_C"));
	if (Anim_BP.Succeeded())
		GetMesh()->SetAnimInstanceClass(Anim_BP.Class);
	GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -97.0));
	GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0, 0.0f));
	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetRelativeLocation(FVector(5.087819, 2.594281, -8.302563));
	FollowCamera->SetRelativeRotation(FRotator(-2.759561, 89.141350, -93.563515));
	FollowCamera->SetupAttachment(GetMesh(), "head");// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//set FlashLight
	FlashLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("FlashLight"));
	/*FlashLight->SetRelativeLocation(FVector(0, 30, 160)); 
	FlashLight->SetRelativeRotation(FRotator(0, 90, 0));*/
	FlashLight->Intensity = 20000;
	FlashLight->AttenuationRadius = 2000;
	FlashLight->InnerConeAngle = 22.0;
	FlashLight->SetupAttachment(FollowCamera);
	FlashLight->SetVisibility(false);
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//Set PlayerProperty
	Hunger = 100;
	HungerDownRate = 1.25;
	Stamina = 100;
	StaminaDownRate = 5;
	StaminaUpRate = 2;
	Sprinting = false;
	Battery = 100;
	BatteryDownRate = 1;
	IsFlashLightOn = false;
	EquipedItems.Init(NULL, 5);
	bIsEquipedItemChanged = false;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASurvivalHorrorCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASurvivalHorrorCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASurvivalHorrorCharacter::StopSprint);

	PlayerInputComponent->BindAction("FlashLightOn", IE_Pressed, this, &ASurvivalHorrorCharacter::FlashLightSwitch);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASurvivalHorrorCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASurvivalHorrorCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASurvivalHorrorCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASurvivalHorrorCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ASurvivalHorrorCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ASurvivalHorrorCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ASurvivalHorrorCharacter::OnResetVR);
}

void ASurvivalHorrorCharacter::Tick(float dt)
{
	if (Hunger > 0)
	{
		Hunger -= HungerDownRate * dt;
		//UE_LOG(LogTemp, Warning, TEXT("PlayerHunger : %f"), PlayerHunger);
	}
	else
	{
		UGameplayStatics::SetGamePaused(this, true);
		//UE_LOG(LogTemp, Warning, TEXT("YouDead"));
	}
	if (Stamina < 100)
	{
		Stamina += StaminaUpRate * dt;
		Stamina = (Stamina > 100) ? 100 : Stamina;
	}
	if (Sprinting)
	{
		if (Stamina >= 0)
		{
			Stamina -= StaminaDownRate * dt;
			Stamina = (Stamina < 0) ? 0 : Stamina;
		}
		else
		{
			StopSprint();
		}
	}

	if(IsFlashLightOn)
	{
		if (Battery <= 0)
		{
			FlashLightSwitch();
		}else
		{
			Battery -= BatteryDownRate * dt;
		}
		
	}
}

void ASurvivalHorrorCharacter::BeginPlay()
{
	Super::BeginPlay();
	/*for (int32 i = 0; i < 5; i++)
	{
		EquipedItems.Emplace(NewObject<UEquipedItem_Base>());
	}*/
	
}


void ASurvivalHorrorCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ASurvivalHorrorCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void ASurvivalHorrorCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void ASurvivalHorrorCharacter::Sprint()
{
	if (Stamina > 0) {
		Sprinting = true;
		GetCharacterMovement()->MaxWalkSpeed = 1200;
	}
}

void ASurvivalHorrorCharacter::StopSprint()
{
	Sprinting = false;
	GetCharacterMovement()->MaxWalkSpeed = 600;
}

void ASurvivalHorrorCharacter::FlashLightSwitch()
{
	IsFlashLightOn = !IsFlashLightOn;
	FlashLight->SetVisibility(IsFlashLightOn);
}

void ASurvivalHorrorCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASurvivalHorrorCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASurvivalHorrorCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ASurvivalHorrorCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

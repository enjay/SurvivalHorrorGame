// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_PickUp2.h"
#include "Engine/StaticMesh.h"
#include "Materials/Material.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"

AFood_PickUp2::AFood_PickUp2()
{
	//Set StaticMeshComponent
	ConstructorHelpers::FObjectFinder<UMaterial>Material(TEXT("/Game/Textures/MinorPickup_Diffuse_Mat"));
	if (Material.Succeeded())
		StaticMeshComponent->SetMaterial(0, Material.Object);
	//Set Food_Pickup Value
	addHunger = 10;
}

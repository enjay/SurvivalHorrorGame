// Fill out your copyright notice in the Description page of Project Settings.


#include "Logs_EquipedItem.h"
#include "Engine/Texture2D.h"
#include "Item_PickUp_Base.h"
#include "UObject/ConstructorHelpers.h"

ULogs_EquipedItem::ULogs_EquipedItem()
{
	ConstructorHelpers::FObjectFinder<UTexture2D>tmpTexture(TEXT("/Game/HUDAssets/LogsInvItem"));
	if (tmpTexture.Succeeded())
		texture = tmpTexture.Object;
}
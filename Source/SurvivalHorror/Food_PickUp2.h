// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food_PickUp1.h"
#include "Food_PickUp2.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API AFood_PickUp2 : public AFood_PickUp1
{
	GENERATED_BODY()
public:
	AFood_PickUp2();
};

// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SurvivalHorrorCharacter.generated.h"

UCLASS(config = Game)
class ASurvivalHorrorCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	/** FlashLight */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpotLightComponent* FlashLight;
public:
	ASurvivalHorrorCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	//Player Property
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		float Hunger;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		float HungerDownRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		float Stamina;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		float StaminaDownRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		float StaminaUpRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		bool Sprinting;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		float Battery;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		float BatteryDownRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		bool IsFlashLightOn;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerProperty")
		TArray<class UEquipedItem_Base*> EquipedItems;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PlayerProperty")
		bool bIsEquipedItemChanged;
protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void Sprint();
	void StopSprint();
	void FlashLightSwitch();
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface
	virtual void Tick(float dt) override;

	virtual void BeginPlay() override;
public:
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};


// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectiveChange_Base.h"
#include "Engine/StaticMesh.h"
#include "Materials/Material.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "SurvivalHorrorCharacter.h"
#include "SurvivalSaveGame.h"
#include "Kismet/GameplayStatics.h"

AObjectiveChange_Base::AObjectiveChange_Base()
{
	//Set StaticMeshComponent
	ConstructorHelpers::FObjectFinder<UStaticMesh>StaticMesh(TEXT("/Game/StarterContent/Shapes/Shape_TriPyramid"));
	if (StaticMesh.Succeeded())
		StaticMeshComponent->SetStaticMesh(StaticMesh.Object);
	ConstructorHelpers::FObjectFinder<UMaterial>Material(TEXT("/Game/StarterContent/Materials/M_Wood_Oak"));
	if (Material.Succeeded())
		StaticMeshComponent->SetMaterial(0, Material.Object);
	//Set ObjectChange Value
	ObjectText = "Objective1:Find The Key";
	rotateRate = 30;
}

void AObjectiveChange_Base::OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ASurvivalHorrorCharacter *Character = Cast<ASurvivalHorrorCharacter>(OtherActor);
	if (Character)
	{
		SaverSubClass = (UGameplayStatics::DoesSaveGameExist("SurvivalSaveSlot", 0)) ? UGameplayStatics::LoadGameFromSlot("SurvivalSaveSlot", 0) : UGameplayStatics::CreateSaveGameObject(USurvivalSaveGame::StaticClass());
		USurvivalSaveGame *SurvivalSaveGameObject= Cast<USurvivalSaveGame>(SaverSubClass);
		SurvivalSaveGameObject->PlayerObject = ObjectText;
		UGameplayStatics::SaveGameToSlot(SaverSubClass,"SurvivalSaveSlot", 0);
	}
}

void AObjectiveChange_Base::Tick(float dt)
{
	AddActorWorldRotation(dt*FRotator(0.0f,1.0f,0.0f)*rotateRate);
}

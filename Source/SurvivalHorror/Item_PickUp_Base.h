// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp_Base.h"
#include "Item_PickUp_Base.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API AItem_PickUp_Base : public APickUp_Base
{
	GENERATED_BODY()
public:
	AItem_PickUp_Base();
protected:
	virtual void OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EquipedItem_Base.h"
#include "Key_EquipedItem.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API UKey_EquipedItem : public UEquipedItem_Base
{
	GENERATED_BODY()
public:
	UKey_EquipedItem();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipedItem_Base.h"
#include "Engine/Texture2D.h"
#include "Item_PickUp_Base.h"
#include "UObject/ConstructorHelpers.h"

UEquipedItem_Base::UEquipedItem_Base()
{
	ConstructorHelpers::FObjectFinder<UTexture2D>tmpTexture(TEXT("/Game/HUDAssets/ArrowUIIcon"));
	if (tmpTexture.Succeeded())
		texture = tmpTexture.Object;

}
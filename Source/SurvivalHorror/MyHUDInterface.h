// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MyHUDInterface.generated.h"

/**
 *
 */
class UUserWidget;
UCLASS()
class SURVIVALHORROR_API AMyHUDInterface : public AHUD
{
	GENERATED_BODY()
public:
	UUserWidget* MyHUD;
	void BeginPlay() override;
};

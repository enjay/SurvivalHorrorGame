// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp_Base.h"
#include "Components/StaticMeshComponent.h"
// Sets default values
APickUp_Base::APickUp_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//Set StaticMeshComponent
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetCollisionProfileName("OverlapAll");
	StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &APickUp_Base::OnOverLap);
	RootComponent=StaticMeshComponent;
}

// Called when the game starts or when spawned
void APickUp_Base::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickUp_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUp_Base::OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("PickUp Overlapped"));
}


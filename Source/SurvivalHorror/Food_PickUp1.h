// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp_Base.h"
#include "Food_PickUp1.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API AFood_PickUp1 : public APickUp_Base
{
	GENERATED_BODY()
public:
	AFood_PickUp1();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PickUpValue", meta = (AllowPrivateAccess = "true"))
		float addHunger;
		virtual void OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Item_PickUp_Base.h"
#include "SurvivalHorrorCharacter.h"
#include "EquipedItem_Base.h"

AItem_PickUp_Base::AItem_PickUp_Base()
{	
}


void AItem_PickUp_Base::OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ASurvivalHorrorCharacter *Character = Cast<ASurvivalHorrorCharacter>(OtherActor);
	if (Character)
	{
		for(UEquipedItem_Base *&item: Character->EquipedItems)
		{
			if(item==NULL)
			{
				item = NewObject<UEquipedItem_Base>();
				Character->bIsEquipedItemChanged = true;
				Destroy();
				break;
			}
		}
	}
}



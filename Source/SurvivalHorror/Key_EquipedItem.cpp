// Fill out your copyright notice in the Description page of Project Settings.


#include "Key_EquipedItem.h"
#include "Engine/Texture2D.h"
#include "Item_PickUp_Base.h"
#include "UObject/ConstructorHelpers.h"

UKey_EquipedItem::UKey_EquipedItem()
{
	ConstructorHelpers::FObjectFinder<UTexture2D>tmpTexture(TEXT("/Game/HUDAssets/KeyInvItem"));
	if (tmpTexture.Succeeded())
		texture = tmpTexture.Object;
}
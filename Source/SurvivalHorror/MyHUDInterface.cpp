// Fill out your copyright notice in the Description page of Project Settings.


#include "MyHUDInterface.h"
#include "Blueprint/UserWidget.h"
#include "SurvivalHorrorCharacter.h"

void AMyHUDInterface::BeginPlay()
{
	Super::BeginPlay();
	TSubclassOf<UUserWidget>MainHUD = LoadClass<UUserWidget>(nullptr, TEXT("WidgetBlueprint'/Game/ThirdPersonCPP/Blueprints/MainHUD.MainHUD_C'"));
	if (MainHUD)
	{
		UE_LOG(LogTemp, Warning, TEXT("LoadClass MyHUD Success"));
		MyHUD = CreateWidget<UUserWidget>(GetWorld(), MainHUD);
		MyHUD->AddToViewport();
	}
	
}

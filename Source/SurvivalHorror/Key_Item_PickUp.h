// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item_PickUp_Base.h"
#include "Key_Item_PickUp.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API AKey_Item_PickUp : public AItem_PickUp_Base
{
	GENERATED_BODY()
public:
	AKey_Item_PickUp();
protected:
	virtual void OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;

};

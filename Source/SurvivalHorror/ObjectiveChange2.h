// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ObjectiveChange_Base.h"
#include "ObjectiveChange2.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API AObjectiveChange2 : public AObjectiveChange_Base
{
	GENERATED_BODY()
public:
	AObjectiveChange2();
};

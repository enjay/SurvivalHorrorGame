// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectiveChange2.h"
#include "Engine/StaticMesh.h"
#include "Materials/Material.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"

AObjectiveChange2::AObjectiveChange2()
{
	//Set StaticMeshComponent
	ConstructorHelpers::FObjectFinder<UMaterial>Material(TEXT("/Game/StarterContent/Props/Materials/M_Shelf"));
	if (Material.Succeeded())
		StaticMeshComponent->SetMaterial(0, Material.Object);
	//Set ObjectChange Value
	ObjectText = "Objective2:Kill The Emeny";
	rotateRate = 50;
}
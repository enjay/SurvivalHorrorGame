// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SurvivalSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API USurvivalSaveGame : public USaveGame
{
	GENERATED_BODY()
public:
	USurvivalSaveGame();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerObject", meta = (AllowPrivateAccess = "true"))
		FString PlayerObject;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp_Base.h"
#include "ObjectiveChange_Base.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API AObjectiveChange_Base : public APickUp_Base
{
	GENERATED_BODY()
public:
	AObjectiveChange_Base();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ObjectChangeValue", meta = (AllowPrivateAccess = "true"))
		float rotateRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ObjectChangeValue", meta = (AllowPrivateAccess = "true"))
		FString ObjectText;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ObjectChangeValue", meta = (AllowPrivateAccess = "true"))
		class USaveGame* SaverSubClass;
	virtual void OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;
protected:
	virtual void Tick(float dt) override;
};

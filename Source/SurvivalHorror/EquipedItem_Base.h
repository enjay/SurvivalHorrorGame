// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "EquipedItem_Base.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALHORROR_API UEquipedItem_Base : public UObject
{
	GENERATED_BODY()
public:
	UEquipedItem_Base();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EquipedItem", meta = (AllowPrivateAccess = "true"))
		UTexture2D *texture;
	

};

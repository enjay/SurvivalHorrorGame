// Fill out your copyright notice in the Description page of Project Settings.


#include "Battery_PickUp.h"
#include "Engine/StaticMesh.h"
#include "Materials/Material.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "SurvivalHorrorCharacter.h"

ABattery_PickUp::ABattery_PickUp()
{
	//Set StaticMeshComponent
	ConstructorHelpers::FObjectFinder<UStaticMesh>StaticMesh(TEXT("/Engine/BasicShapes/Cube"));
	if (StaticMesh.Succeeded())
		StaticMeshComponent->SetStaticMesh(StaticMesh.Object);
	ConstructorHelpers::FObjectFinder<UMaterial>Material(TEXT("/Game/Textures/BatterPickup_Mat"));
	if (Material.Succeeded())
		StaticMeshComponent->SetMaterial(0, Material.Object);
	//Set Battery_Pickup Value
	addBattery = 20;
}

void ABattery_PickUp::OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ASurvivalHorrorCharacter *Character = Cast<ASurvivalHorrorCharacter>(OtherActor);
	if (Character)
	{
		if (Character->Battery < 100)
		{
			Character->Battery += addBattery;
			Character->Battery = (Character->Battery > 100) ? 100 : Character->Battery;
			Destroy();
		}
	}
}

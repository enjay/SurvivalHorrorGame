// Fill out your copyright notice in the Description page of Project Settings.


#include "Logs_Item_PickUp.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "SurvivalHorrorCharacter.h"
#include "Logs_EquipedItem.h"

ALogs_Item_PickUp::ALogs_Item_PickUp()
{
	//Set StaticMeshComponent
	ConstructorHelpers::FObjectFinder<UStaticMesh>StaticMesh(TEXT("/Game/Textures/Logs_Mesh"));
	if (StaticMesh.Succeeded())
		StaticMeshComponent->SetStaticMesh(StaticMesh.Object);
	SetActorRelativeScale3D(FVector(5,5,5));
}

void ALogs_Item_PickUp::OnOverLap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ASurvivalHorrorCharacter *Character = Cast<ASurvivalHorrorCharacter>(OtherActor);
	if (Character)
	{
		for (UEquipedItem_Base *&item : Character->EquipedItems)
		{
			if (item == NULL)
			{
				item = NewObject<ULogs_EquipedItem>();
				UE_LOG(LogTemp, Warning, TEXT("Logs PickUped"));
				Character->bIsEquipedItemChanged = true;
				Destroy();
				break;
			}
		}
	}
}
